This directory contains a set of templates for the Fachbeirat posters.
     They are ready as-is for the CW group (and the E@H posters in particular),
     and should be modified for other groups;
     in particular, the main color should be changed.
The CW group voted and we will use the 'redTop' (not the 'whiteTop') version.
You may need to pdflatex twice, the usual latex stuff, blah blah


Note: This template is easy to use but also very particular;
     it likes boxes and dislikes too much customization.
     You are welcome to start from somewhere else so long as the end result looks the same.
     

See the bottom of this document for the minimal information you will need;
     this applies only to the CW group;
     everyone else will have to do extra work;
     but this is #notmyproblem;

    - sjzhu



== List of files ==
- essential
     CW_posterTemplate_redTop.tex    final latex template for CW group
     CW_posterTemplate_redTop.pdf    `pdflatex CW_posterTemplate_redTop.tex`
     baposter.cls		     'baposter' class (see details below)
     CW_bkgd_EatH.pdf	    	     red title bar for poster background with E@H logo (see .odg file below)
     CW_bkgd.pdf		     poster bkgd w/o E@H logo
     luh_logo_cmyk.png		     LUH logo, made from official pdf version
     MaxPlanck_Minerva_cropped_whitelines.png	version of Minerva used for redTop.tex
     aei_logo_whitebkgd.png	     MPG 'g' with red and white inverted, used for redTop.tex
     Neutron_star_illustrated.jpg    an example figure in the template
- extra / customizable
     CW_posterTemplate_whiteTop.tex  alternative version; voted down
     CW_posterTemplate_whiteTop.pdf  pdflatex-ified from .tex file
     CW_bkgd_EatH.odg		     red title bar for bkgd with E@H logo, made in Open Office Draw
     CW_bkgd.odg		     red title bar for bkgd, made in Open Office Draw
     luh_logo_cmyk.pdf		     LUH logo (from https://wolke7.aei.mpg.de/index.php/s/bea2oJkdM2oswsq)
     MaxPlanck_Minerva.png	     original Minerva, from Wikipedia
     MaxPlanck_Minerva_cropped<etc>  exactly what the name says
     aei_logo.png		     original MPG 'g' with correct colors


== 'baposter' class ==   [ http://www.brian-amberg.de/uni/poster/ ]
   Description from website:
   	A LaTeX template to efficently design pretty posters for scientific conferences.
	Posters are composited of blocks with headings, which can be positioned easily
	on the page, using absolute or relative positioning. A number of predefined styles
	can be composed to generate new color schemes and ornaments.

   Summary
	The baposter class is built out of blocks. 
	Each block has a header (the title of the block)
	  and a body (the text, figures, etc.).
	Most (all?) normal latex options will work for the block bodies
	  (e.g., equations, figures, tables, captions, etc.)
	The template is currently made of 3 columns and contains six blocks.
	Hopefully, the options and descriptions are clear; if not, ask me (sjz) or the internet.
   Working with the boxes
   	(Disclaimer: This is only what I know from playing around with the template, you should trust but verify)
	\headerbox{ [HEADER TEXT GOES HERE] }
	next comes box sizing and positioning
	     {
	     name = the box label, since boxes' vertical positions are usually defined wrt other boxes
	     column = which column the box starts in (think latex tables), 0-indexed
	     span = how many columns the box should span (think latex tables)
	     row = position is either absolute (row=0, row=1) or in relation to others (see 'below')
	     below = the name of the box that this appears directly underneath
         bottomaligned = the name of the box to align the bottom of this box with (see note below)
	     }
	then whatever content you want in that particular box
	     {blah blah normal texy things}
	NOTE: You can specify the aesthetics of a particular box individually. See, e.g.:
	http://tex.stackexchange.com/questions/131328/footer-text-under-poster
    NOTE: In order to align the bottom edged of boxes, use the bottomaligned
          option with the name of the box to align to; for a better explanation
          see http://tex.stackexchange.com/questions/175474/aligned-two-headerbox-at-the-bottom



== Note for non-CW groups ==
   You will probably need to change the color scheme from red to not-red. The colors appear in the following places:
        \definecolor{bordercol}
	\definecolor{headercol2}
	\definecolor{headerfontcol}
	CW_bkgd<whatever>.odg, which is literally just a red rectangle made in Open Office Draw
           plus 'Searching for Continuous Waves' at the top and (sometimes) the E@H logo at the bottom
	   ==> make sure to export as pdf and use the pdf version as the bkgd in the latex file
   You will probably also want to use the regular MPG 'g' and not the inverted one




== Minimal information (for the lazy), i.e., Here are the parts you need to change in the template for your poster:
        title; after \begin{poster}
	authors; right after title
	stuff that goes in the boxes (see 'Working with the boxes' above for more details)
	the background pdf, if you're not using Einstein@Home (current version in template includes E@H logo)
