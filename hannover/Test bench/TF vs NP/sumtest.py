import tensorflow as tf
import numpy

#defining a 800Mb matrix of random generated data
npoints = numpy.int(2e6)
nsegments = numpy.int(1e2)

fStatMatrix = numpy.random.rand(npoints,nsegments)
fStatMatrix = fStatMatrix.astype(numpy.float32)
print(fStatMatrix.nbytes/1e6)

def sumNP(matrix):
    sums = numpy.sum(matrix, axis = 1)
    return sums

def sumTF(matrix):
    matrixTF = tf.constant(matrix)
    sums = tf.reduce_sum(matrixTF, axis = 1)
    return sums

import time

startNP = time.time()
sumArray = sumNP(fStatMatrix)
stopNP = time.time()

sess = tf.Session()
sumTensor = sumTF(fStatMatrix)
startTF = time.time()
sumTensor = sess.run(sumTensor)
stopTF = time.time()

print(sumTensor.shape)
print(stopNP-startNP)
print(stopTF-startTF)
