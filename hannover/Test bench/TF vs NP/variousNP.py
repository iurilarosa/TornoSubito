import numpy
import time


def matgen(nrows, ncolumns, ndet):
    ranmatr = numpy.random.rand(nrows,ncolumns, ndet)
    ranmatr = ranmatr.astype(numpy.float32)
    return ranmatr

npoints = 10000
nsegments = 100
nmatrices = 3

fmatr1 = matgen(npoints,nsegments,nmatrices)

start = time.time()
summ = numpy.sum(fmatr1, axis = 1)
maxx = numpy.amax(fmatr1, axis = 1)
print(time.time()-start)
