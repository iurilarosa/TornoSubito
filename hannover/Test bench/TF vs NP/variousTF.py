import numpy
import tensorflow as tf
import time 

sess = tf.Session()

npoints = 10000
nsegments = 100
nmatrices = 3

targetPoints = tf.random_uniform([nsegments], 0, npoints, dtype = tf.int32)
targetRows = tf.range(nsegments, dtype = tf.int32)
targets = tf.stack([targetPoints,targetRows], axis = 0)

fmatr1 = tf.random_uniform((npoints, nsegments))
ranmat = sess.run(fmatr1)

#summ = tf.reduce_sum(fmatr1, axis = 1)
maxx = tf.reduce_max(fmatr1, axis = 1)

summ = tf.unsorted_segment_sum(fmatr1, targets, nsegments)

stats = tf.stack([summ,maxx],0)

start = time.time()
stated = sess.run(stats)
print(time.time()-start)
print(stated.shape, stated.size, stated.nbytes/1e6)
