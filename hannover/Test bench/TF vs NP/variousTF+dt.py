import numpy
import tensorflow as tf
import time 

sess = tf.Session()

def matgen(nrows, ncolumns, ndet):
    ranmatr = numpy.random.rand(nrows,ncolumns, ndet)
    ranmatr = ranmatr.astype(numpy.float32)
    return ranmatr



npoints = 10000
nsegments = 100
nmatrices = 3

fmatr1 = matgen(npoints,nsegments,nmatrices)

nNewPoints = 10
newPoints = numpy.random.randint(0, npoints, nNewPoints)
newPoints = numpy.sort(newPoints).astype(numpy.int32)

newRows = matgen(nNewPoints,nsegments,nmatrices).astype(numpy.float32)
print(newRows)
#TF FROM HERE
#con scatter
#newIndices = tf.zeros(nNewPoints,nsegments,nmatrices)

#fmatr1TF = tf.Variable(fmatr1)




#summ = tf.reduce_sum(fmatr1TF, axis = 1)
#maxx = tf.reduce_max(fmatr1TF, axis = 1)

#stats = tf.stack([summ,maxx],0)

#start = time.time()
#stated = sess.run(stats)
#print(time.time()-start)
#print(stated.shape, stated.size, stated.nbytes/1e6)





# 0 [ ]
# study tf in C such that I can work directly with the original cose
# (it is necessary to understand ho many new data are computed in the if in the main loop)

# 1 [ ]
# try to vary some rows in a loop, using tf Variable
# inspect how changes the data transfer overhead with respect to the variation of the number or new row

# 2 [ ]
# sums with unsorted segmented sum or some kind of scatter sum instead that on a row

# 3 [ ]
# understand realistic execution time of weave  
# run numpy test in same machine where weave was testet (how many GHz clock?)

