 
 
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

int nElements = 1000*1000;

int main()
{
	float array[nElements];
	float sum, value;
	float random;
	float integral;
	float start,end,timing;
	
	for(int i = 0; i < nElements; i++)
	{
		array[i] = (float)rand()/RAND_MAX;
// 		printf("%f\n", array[i]);
	}
	
	start = clock();
	
	sum = 0;
	for(int i = 0; i < nElements; i++)
	{
		sum = sum + array[i];
	}
	
	end = clock();
	
	timing = (float)(end - start) / CLOCKS_PER_SEC;
	printf("Time spent is %f s,\n", timing);
	printf("Sum of array is %f.\n", sum);
	printf("end\n");
	
	return 0;
}
