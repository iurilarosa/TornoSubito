 
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

int nElements = 100000;

float integration(float *array, int size) 
{
	float sum;
	float value;
	sum = 0;
   
	for(int i = 0; i < size; i++) 
	{
		value = *array;
		sum = sum + value;
		array++;
	}
	
     

   return sum;
}


float randomgen(int j)
{
	float random;

	random = (float)rand()/RAND_MAX;
	return random;
}


int main()
{
	
	float integrala;//, integralb, integralc, integrald;
	float a[nElements];//, b[nElements], c[nElements], d[nElements];
	float start, end, timing;
	
	
	for(int i = 0; i < nElements; i++)
	{
		a[i] = randomgen(i);
// 		b[i] = randomgen(i);
// 		c[i] = randomgen(i);
// 		d[i] = randomgen(i);
	}
	start = clock();
	integrala = integration(a, nElements)/1000;
// 	integralb = integration(b, nElements)/1000;
// 	integralc = integration(c, nElements)/1000;
// 	integrald = integration(d, nElements)/1000;
	end = clock();
	
	timing = (float)(end - start) / CLOCKS_PER_SEC;
	
	printf("Time spent is %f s,\n", timing);
	printf("Sum of array is %fe3.\n", integrala);
	printf("end\n");
	
	return 0;
}

