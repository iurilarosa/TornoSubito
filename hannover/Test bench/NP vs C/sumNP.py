import numpy
import time

N = 100000


matr = numpy.random.rand(N)
startuni=time.time()
summ = numpy.sum(matr)

stopuni = time.time()

print(stopuni-startuni)
print(summ)
