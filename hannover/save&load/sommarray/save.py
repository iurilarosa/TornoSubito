import tensorflow as tf
import numpy as np
tf.reset_default_graph()
size = 100
x = tf.placeholder(tf.float32, shape=[1, size], name='input')

zeri = np.zeros([1,size]).astype(np.float32)
variab = tf.Variable(zeri, name='var')
sommo = tf.add(x,variab) 
output = tf.reduce_sum(sommo, name='output')


val = np.random.rand(1,size)


with tf.Session() as sess:
    sess.run(tf.global_variables_initializer())

    # save graph
    saver = tf.train.Saver(tf.global_variables())
    saver.save(sess, './exported/my_model')

    tf.train.write_graph(sess.graph, '.', "./exported/graph.txt")
    #tf.train.write_graph(sess.graph, '.', "./exported/graph.pb_txt", as_text=True)

    x1 = tf.get_default_graph().get_tensor_by_name('input:0')
    t1 = tf.get_default_graph().get_tensor_by_name('output:0')

    lalala= sess.run(t1, feed_dict={x1: val})

    print(tf.global_variables())
    print("out           " ,lalala)
