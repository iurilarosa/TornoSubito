import numpy
import tensorflow as tf
import time 

tf.reset_default_graph()
sess = tf.Session()

rows = 10000
columns = 100
matrices = 3

inputto = tf.random_uniform((rows,columns,matrices), name= "input")

#fmatr1 = tf.get_variable("matrix", shape=(rows,columns,matrices))
#fmatr1 = fmatr1.assign(inputto)
zeri = tf.zeros([rows,columns,matrices])
fmatr1 = tf.Variable(zeri)
fmatr1 = fmatr1.assign(inputto)

summ = tf.reduce_sum(fmatr1, axis = 1)
maxx = tf.reduce_max(fmatr1, axis = 1)

stats = tf.stack([summ,maxx],0, name="output")
sess.run(tf.global_variables_initializer())
start=time.time()
sess.run(stats)
print(time.time()-start)
