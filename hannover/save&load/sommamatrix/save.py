import tensorflow as tf
import numpy as np
tf.reset_default_graph()

npoints = 10000
nsegments = 100
nmatrices = 3
size = npoints * nsegments * nmatrices

x = tf.placeholder(tf.float32, shape=[npoints, nsegments, nmatrices], name='input')

zeri = np.zeros([npoints, nsegments, nmatrices]).astype(np.float32)
variab = tf.Variable(zeri, name='var')
sommo = tf.add(x,variab) 
output = tf.reduce_sum(sommo, axis = 1, name='output')


val = np.random.rand(npoints, nsegments, nmatrices)


with tf.Session() as sess:
    sess.run(tf.global_variables_initializer())

    # save graph
    saver = tf.train.Saver(tf.global_variables())
    saver.save(sess, './exported/my_model')

    tf.train.write_graph(sess.graph, '.', "./exported/graph.pb", as_text=False)
    #tf.train.write_graph(sess.graph, '.', "./exported/graph.pb_txt", as_text=True)

    x1 = tf.get_default_graph().get_tensor_by_name('input:0')
    t1 = tf.get_default_graph().get_tensor_by_name('output:0')

    lalala= sess.run(t1, feed_dict={x1: val})

    print(tf.global_variables())
    print("out           " ,lalala)
