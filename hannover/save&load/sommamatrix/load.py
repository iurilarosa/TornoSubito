import tensorflow as tf
import numpy as np
tf.reset_default_graph()

npoints = 10000
nsegments = 100
nmatrices = 3
val = np.random.rand(npoints, nsegments, nmatrices)

with tf.Session() as sess:

    # load the computation graph
    loader = tf.train.import_meta_graph('./exported/my_model.meta')
    sess.run(tf.global_variables_initializer())
    loader = loader.restore(sess, './exported/my_model')

    x1 = tf.get_default_graph().get_tensor_by_name('input:0')
    t1 = tf.get_default_graph().get_tensor_by_name('output:0')
    
    lalala= sess.run(t1, feed_dict={x1: val})

    print(tf.global_variables())
    print("out           " ,lalala)
