// Patrick Wieschollek <mail@patwie.com>
#include <time.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <tensorflow/c/c_api.h>

TF_Buffer* read_file(const char* file);

void free_buffer(void* data, size_t length) {
  free(data);
}

void deallocator(void* ptr, size_t len, void* arg) {
  free((void*) ptr);
}


int main(int argc, char const *argv[]) {
  // load graph
  // ================================================================================
  TF_Buffer* graph_def = read_file("./exported/graph.pb");
  TF_Graph* graph = TF_NewGraph();
  TF_Status* status = TF_NewStatus();
  TF_ImportGraphDefOptions* opts = TF_NewImportGraphDefOptions();
  TF_GraphImportGraphDef(graph, graph_def, opts, status);
  TF_DeleteImportGraphDefOptions(opts);
  if (TF_GetCode(status) != TF_OK) {
    fprintf(stderr, "ERROR: Unable to import graph %s\n", TF_Message(status));
    return 1;
  }
  fprintf(stdout, "Successfully imported graph\n");


  // create session
  // ================================================================================
  TF_SessionOptions* opt = TF_NewSessionOptions();
  TF_Session* sess = TF_NewSession(graph, opt, status);
  TF_DeleteSessionOptions(opt);

  // run init operation
  // ================================================================================
  const TF_Operation *init_op = TF_GraphOperationByName(graph, "init");
  const TF_Operation* const* targets_ptr = &init_op;

  TF_SessionRun(sess,
                /* RunOptions */         NULL,
                /* Input tensors */      NULL, NULL, 0,
                /* Output tensors */     NULL, NULL, 0,
                /* Target operations */  targets_ptr, 1,
                /* RunMetadata */        NULL,
                /* Output status */      status);

  // run restore
  // ================================================================================
  TF_Operation *checkpoint_op = TF_GraphOperationByName(graph, "save/Const");
  TF_Operation *restore_op = TF_GraphOperationByName(graph, "save/restore_all");

  char* checkpoint_path_str = "./exported/my_model";
  size_t checkpoint_path_str_len = strlen(checkpoint_path_str);
  size_t encoded_size = TF_StringEncodedSize(checkpoint_path_str_len);

  size_t total_size = sizeof(int64_t) + encoded_size;
  char *input_encoded = (char*)malloc(total_size);
  memset(input_encoded, 0, total_size);
  TF_StringEncode(checkpoint_path_str, checkpoint_path_str_len, input_encoded + sizeof(int64_t), encoded_size, status);

  TF_Tensor* path_tensor = TF_NewTensor(TF_STRING, NULL, 0, input_encoded, total_size, &deallocator, 0);

  TF_Output* run_path = (TF_Output*)malloc(1 * sizeof(TF_Output));
  run_path[0].oper = checkpoint_op;
  run_path[0].index = 0;

  TF_Tensor** run_path_tensors = (TF_Tensor**)malloc(1 * sizeof(TF_Tensor*));
  run_path_tensors[0] = path_tensor;

  TF_SessionRun(sess,
                /* RunOptions */         NULL,
                /* Input tensors */      run_path, run_path_tensors, 1,
                /* Output tensors */     NULL, NULL, 0,
                /* Target operations */  &restore_op, 1,
                /* RunMetadata */        NULL,
                /* Output status */      status);

// prepare inputs
  int npoints = 100;
  int nsegments = 100;
  int nmatrices = 3;
  int size = npoints * nsegments * nmatrices;
  int i;
  float random = RAND_MAX;
  TF_Operation *input_op = TF_GraphOperationByName(graph, "input");
  printf("input_op has %i inputs\n", TF_OperationNumOutputs(input_op));
  float* raw_input_data = (float*)malloc(size * sizeof(float));
  for(i = 0; i < size; i++)
    raw_input_data[i] = rand() / random;
  int64_t* raw_input_dims = (int64_t*)malloc(3 * sizeof(int64_t));
  raw_input_dims[0] = npoints;
  raw_input_dims[1] = nsegments;
  raw_input_dims[2] = nmatrices;

  TF_Tensor* input_tensor = TF_NewTensor(TF_FLOAT,
                                         raw_input_dims, 3,
                                         raw_input_data, size * sizeof(float),
                                         &deallocator,
                                         NULL
                                        );
 
  TF_Output* run_inputs = (TF_Output*)malloc(1 * sizeof(TF_Output));
  run_inputs[0].oper = input_op;
  run_inputs[0].index = 0;

  TF_Tensor** run_inputs_tensors = (TF_Tensor**)malloc(1 * sizeof(TF_Tensor*));
  run_inputs_tensors[0] = input_tensor;

  // prepare outputs
  // ================================================================================
  TF_Operation* output_op = TF_GraphOperationByName(graph, "output");
  // printf("output_op has %i outputs\n", TF_OperationNumOutputs(output_op));

  TF_Output* run_outputs = (TF_Output*)malloc(1 * sizeof(TF_Output));
  run_outputs[0].oper = output_op;
  run_outputs[0].index = 0;

  int sizeOut = npoints * nmatrices;
  TF_Tensor** run_output_tensors = (TF_Tensor**)malloc(1 * sizeof(TF_Tensor*));
  float* raw_output_data = (float*)malloc(sizeOut * sizeof(float));
  for(i = 0; i<sizeOut; i++)
    raw_output_data[0] = 1.f;
  int64_t* raw_output_dims = (int64_t*)malloc(2 * sizeof(int64_t));
  raw_output_dims[0] = npoints;
  raw_output_dims[1] = nmatrices;
  
  TF_Tensor* output_tensor = TF_NewTensor(TF_FLOAT,
                                          raw_output_dims, 2,
                                          raw_output_data, sizeOut * sizeof(float),
                                          &deallocator,
                                          NULL
                                         );
  run_output_tensors[0] = output_tensor;

  // run network
  // ================================================================================
  float start, end, tempizzo;
  start = clock();
  TF_SessionRun(sess,
                /* RunOptions */         NULL,
                /* Input tensors */      run_inputs, run_inputs_tensors, 1,
                /* Output tensors */     run_outputs, run_output_tensors, 1,
                /* Target operations */  NULL, 0,
                /* RunMetadata */        NULL,
                /* Output status */      status);

  // printf("output-tensor has %i dims\n", TF_NumDims(run_output_tensors[0]));

  end = clock();
  tempizzo = (float)(end - start) / CLOCKS_PER_SEC;
  printf("%d random numbers gen time %f s\n", size, tempizzo);
  
  void* output_data = TF_TensorData(run_output_tensors[0]);
  size_t dim_out = TF_TensorByteSize(run_output_tensors[0]);
  int num_dim = TF_NumDims(run_output_tensors[0]);
  for(i = 0; i < npoints; i++)
    printf("%d\t%f\t%f\t%f\n",i,((float*)output_data)[i],((float*)output_data)[i+1],((float*)output_data)[i+2]);
  printf("\n dim %d\n", num_dim);
  printf("ndim %d\n", dim_out/4);
  
  
//  for(i = 0; i < sizeOut; i+=3)
//   printf("[%f \t %f \t %f] \n",((float*)output_data)[i],((float*)output_data)[i+1],((float*)output_data)[i+2]);
  // you do not want see me creating all the other tensors; Enough lines for this simple example!


  // free up stuff
  // ================================================================================
  // I probably missed something here
  TF_CloseSession(sess, status);
  TF_DeleteSession(sess, status);

  TF_DeleteStatus(status);
  TF_DeleteBuffer(graph_def);

  TF_DeleteGraph(graph);

  free((void*) input_encoded);
  free((void*) raw_input_data);
  free((void*) raw_input_dims);
  free((void*) run_inputs);
  return 0;
}

TF_Buffer* read_file(const char* file) {
  FILE *f = fopen(file, "rb");
  fseek(f, 0, SEEK_END);
  long fsize = ftell(f);
  fseek(f, 0, SEEK_SET);  //same as rewind(f);

  void* data = malloc(fsize);
  fread(data, fsize, 1, f);
  fclose(f);

  TF_Buffer* buf = TF_NewBuffer();
  buf->data = data;
  buf->length = fsize;
  buf->data_deallocator = free_buffer;
  return buf;
}

