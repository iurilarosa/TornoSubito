
import tensorflow as tf
import numpy as np
from tensorflow.python.client import timeline

tf.reset_default_graph()
sess = tf.Session()
run_options = tf.RunOptions(trace_level=tf.RunOptions.FULL_TRACE)
run_metadata = tf.RunMetadata()


npoints = 100
nsegments = 10
nmatrices = 2
size = npoints * nsegments * nmatrices

x = tf.placeholder(tf.float32, shape=[npoints, nsegments, nmatrices], name='Input_plh')

zeri = np.zeros([npoints, nsegments, nmatrices]).astype(np.float32)
variab = tf.Variable(zeri, name='Variable')
sommo = tf.add(x,variab, name= 'Data_assign') 
output = tf.reduce_sum(sommo, axis = 1, name='Output')


val = np.random.rand(npoints, nsegments, nmatrices)

sess.run(tf.global_variables_initializer())
lalala= sess.run(output, feed_dict={x: val},options=run_options,run_metadata = run_metadata)


logs_path = 'logs'

writer = tf.summary.FileWriter(logdir=logs_path,graph=sess.graph)

time_path = 'logs/timeline.json'
tl = timeline.Timeline(run_metadata.step_stats)
ctf = tl.generate_chrome_trace_format()
with open(time_path, 'w') as f:
    f.write(ctf)

writer.add_run_metadata(run_metadata,"mySess")
writer.close()
sess.close()

 
