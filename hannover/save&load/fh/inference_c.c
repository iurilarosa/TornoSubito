// Patrick Wieschollek <mail@patwie.com>
#include <time.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <tensorflow/c/c_api.h>

TF_Buffer* read_file(const char* file);

void free_buffer(void* data, size_t length) {
  free(data);
}

void deallocator(void* ptr, size_t len, void* arg) {
  free((void*) ptr);
}


int main(int argc, char const *argv[]) {
	// load graph
	// ================================================================================
	TF_Buffer* graph_def = read_file("./exported/graph.pb");
	TF_Graph* graph = TF_NewGraph();
	TF_Status* status = TF_NewStatus();
	TF_ImportGraphDefOptions* opts = TF_NewImportGraphDefOptions();
	TF_GraphImportGraphDef(graph, graph_def, opts, status);
	TF_DeleteImportGraphDefOptions(opts);
	if (TF_GetCode(status) != TF_OK) {
		fprintf(stderr, "ERROR: Unable to import graph %s\n", TF_Message(status));
		return 1;
	}
	fprintf(stdout, "Successfully imported graph\n");


	// create session
	// ================================================================================
	TF_SessionOptions* opt = TF_NewSessionOptions();
	TF_Session* sess = TF_NewSession(graph, opt, status);
	TF_DeleteSessionOptions(opt);
	if (TF_GetCode(status) != TF_OK) {
		fprintf(stderr, "ERROR: Unable to create session %s\n", TF_Message(status));
		return 1;
	}
	fprintf(stdout, "Successfully created session\n");

	// run init operation
	// ================================================================================
	const TF_Operation *init_op = TF_GraphOperationByName(graph, "init");
	const TF_Operation* const* targets_ptr = &init_op;

	TF_SessionRun(sess,
					/* RunOptions */         NULL,
					/* Input tensors */      NULL, NULL, 0,
					/* Output tensors */     NULL, NULL, 0,
					/* Target operations */  targets_ptr, 1,
					/* RunMetadata */        NULL,
					/* Output status */      status);
	if (TF_GetCode(status) != TF_OK) {
		fprintf(stderr, "ERROR: Unable to run init_op: %s\n", TF_Message(status));
		return 1;
	}

	// run restore
	// ================================================================================
	TF_Operation *checkpoint_op = TF_GraphOperationByName(graph, "save/Const");
	TF_Operation *restore_op = TF_GraphOperationByName(graph, "save/restore_all");

	char* checkpoint_path_str = "./exported/my_model";
	size_t checkpoint_path_str_len = strlen(checkpoint_path_str);
	size_t encoded_size = TF_StringEncodedSize(checkpoint_path_str_len);

	// The format for TF_STRING tensors is:
	//   start_offset: array[uint64]
	//   data:         byte[...]
	size_t total_size = sizeof(int64_t) + encoded_size;
	char *input_encoded = (char*)malloc(total_size);
	memset(input_encoded, 0, total_size);
	TF_StringEncode(checkpoint_path_str, checkpoint_path_str_len, input_encoded + sizeof(int64_t), encoded_size, status);
	if (TF_GetCode(status) != TF_OK) {
		fprintf(stderr, "ERROR: something wrong with encoding: %s", TF_Message(status));
		return 1;
	}


	TF_Tensor* path_tensor = TF_NewTensor(TF_STRING, NULL, 0, input_encoded, total_size, &deallocator, 0);

	TF_Output* run_path = (TF_Output*)malloc(1 * sizeof(TF_Output));
	run_path[0].oper = checkpoint_op;
	run_path[0].index = 0;

	TF_Tensor** run_path_tensors = (TF_Tensor**)malloc(1 * sizeof(TF_Tensor*));
	run_path_tensors[0] = path_tensor;

	TF_SessionRun(sess,
					/* RunOptions */         NULL,
					/* Input tensors */      run_path, run_path_tensors, 1,
					/* Output tensors */     NULL, NULL, 0,
					/* Target operations */  &restore_op, 1,
					/* RunMetadata */        NULL,
					/* Output status */      status);
	if (TF_GetCode(status) != TF_OK) {
		fprintf(stderr, "ERROR: Unable to run restore_op: %s\n", TF_Message(status));
		return 1;
	}


	// gerenate input
	// ================================================================================
	FILE *datafile;
	float val;
	int i, j;
	int nsd = 210;
	int size = 4739232;
	int npunti = 1579744;
	int narray = 3;
	
	float *fdots;
	fdots = malloc (sizeof(float) * nsd+1);
	float *dati;
	dati = malloc (sizeof(float) * size);
	
	datafile = fopen("peakmap.txt", "r");
	if (datafile) 
	{
		i = 0;
		while (!feof(datafile)) 
		{
			fscanf(datafile,"%f", &val);
			dati[i] = val;
			i++;
		}
		fclose(datafile);
	}
	
	datafile = fopen("spindowns.txt", "r");
	if (datafile) 
	{
		i = 0;
		while (!feof(datafile)) 
		{
			fscanf(datafile,"%f", &val);
			fdots[i] = val;
			i++;
		}
		fclose(datafile);
	}
	
	TF_Operation *input_opPM = TF_GraphOperationByName(graph, "inputPM");
	printf("input_opPM has %i inputs\n", TF_OperationNumOutputs(input_opPM));
	float* raw_input_dataPM = (float*)malloc(size * sizeof(float));
	for(i = 0; i < size; i++)
		raw_input_dataPM[i] = dati[i];
	int64_t* raw_input_dimsPM = (int64_t*)malloc(2 * sizeof(int64_t));
	raw_input_dimsPM[0] = npunti;
	raw_input_dimsPM[1] = narray;
	
	TF_Operation *input_opSD = TF_GraphOperationByName(graph, "inputSD");
	printf("input_opSD has %i inputs\n", TF_OperationNumOutputs(input_opSD));
	float* raw_input_dataSD = (float*)malloc(nsd * sizeof(float));
	for(i = 0; i < nsd; i++)
		raw_input_dataSD[i] = fdots[i];
	int64_t* raw_input_dimsSD = (int64_t*)malloc(1 * sizeof(int64_t));
	raw_input_dimsSD[0] = nsd;

	
	TF_Tensor* input_tensorPM = TF_NewTensor(TF_FLOAT,
											raw_input_dimsPM, 2,
											raw_input_dataPM, size * sizeof(float),
											&deallocator,
											NULL
											);

	TF_Tensor* input_tensorSD = TF_NewTensor(TF_FLOAT,
											raw_input_dimsSD, 1,
											raw_input_dataSD, nsd * sizeof(float),
											&deallocator,
											NULL
											);



	TF_Output* run_inputs = (TF_Output*)malloc(2 * sizeof(TF_Output));
	run_inputs[0].oper = input_opPM;
	run_inputs[0].index = 0;
	run_inputs[1].oper = input_opSD;
	run_inputs[1].index = 0;

	TF_Tensor** run_inputs_tensors = (TF_Tensor**)malloc(2 * sizeof(TF_Tensor*));
	run_inputs_tensors[0] = input_tensorPM;
	run_inputs_tensors[1] = input_tensorSD;

	// prepare outputs
	// ================================================================================
	int nRows = 210;
	int nColumns = 87652;
	int sizeHM = 18406920;
	
	TF_Operation* output_op = TF_GraphOperationByName(graph, "output");
	// printf("output_op has %i outputs\n", TF_OperationNumOutputs(output_op));

	TF_Output* run_outputs = (TF_Output*)malloc(1 * sizeof(TF_Output));
	run_outputs[0].oper = output_op;
	run_outputs[0].index = 0;

	TF_Tensor** run_output_tensors = (TF_Tensor**)malloc(1 * sizeof(TF_Tensor*));
	float* raw_output_data = (float*)malloc(sizeHM * sizeof(float));
	for(i = 0; i < sizeHM; i++)
		raw_output_data[i] = 1.f;
	int64_t* raw_output_dims = (int64_t*)malloc(2 * sizeof(int64_t));
	raw_output_dims[0] = nRows;
	raw_output_dims[1] = nColumns;

	TF_Tensor* output_tensor = TF_NewTensor(TF_FLOAT,
											raw_output_dims, 2,
											raw_output_data, sizeHM * sizeof(float),
											&deallocator,
											NULL
											);
	run_output_tensors[0] = output_tensor;

	// run network
	// ================================================================================
	float start, end, tempizzo;
	start = clock();
	TF_SessionRun(sess,
					/* RunOptions */         NULL,
					/* Input tensors */      run_inputs, run_inputs_tensors, 2,
					/* Output tensors */     run_outputs, run_output_tensors, 1,
					/* Target operations */  NULL, 0,
					/* RunMetadata */        NULL,
					/* Output status */      status);
	if (TF_GetCode(status) != TF_OK) {
		fprintf(stderr, "ERROR: Unable to run output_op: %s\n", TF_Message(status));
		return 1;
	}
	end = clock();
	tempizzo = (float)(end - start) / CLOCKS_PER_SEC;
	printf("time %f s\n", size, tempizzo);
	// printf("output-tensor has %i dims\n", TF_NumDims(run_output_tensors[0]));
	int n;

	void* output_data = TF_TensorData(run_output_tensors[0]);
	size_t dim_out = TF_TensorByteSize(run_output_tensors[0]);
	int num_dim = TF_NumDims(run_output_tensors[0]);
	printf("output %f\n", ((float*)output_data)[6000]);
	printf("dim %d\n", num_dim);
	printf("ndim %d\n", dim_out/4);
	
//	datafile = fopen("/home/iuri.larosa/data/hough.txt", "w");
//	for(i = 0; i < sizeHM;i++)
//		fprintf(datafile, "%f\t",((float*)output_data)[i]);


	// free up stuff
	// ================================================================================
	// I probably missed something here
	TF_CloseSession(sess, status);
	TF_DeleteSession(sess, status);

	TF_DeleteStatus(status);
	TF_DeleteBuffer(graph_def);

	TF_DeleteGraph(graph);

	free((void*) input_encoded);
	free((void*) raw_input_dataSD);
	free((void*) raw_input_dimsSD);
	free((void*) raw_input_dataPM);
	free((void*) raw_input_dimsPM);
	free((void*) run_inputs);
	return 0;
}

TF_Buffer* read_file(const char* file) {
	FILE *f = fopen(file, "rb");
	fseek(f, 0, SEEK_END);
	long fsize = ftell(f);
	fseek(f, 0, SEEK_SET);  //same as rewind(f);

	void* data = malloc(fsize);
	fread(data, fsize, 1, f);
	fclose(f);

	TF_Buffer* buf = TF_NewBuffer();
	buf->data = data;
	buf->length = fsize;
	buf->data_deallocator = free_buffer;
	return buf;
}
