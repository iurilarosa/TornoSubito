import tensorflow as tf
import numpy
import scipy.io
import time

#---------------------------------------
# defining parameters                   |
#---------------------------------------
percorsoDati = "/home/protoss/Documenti/TESI/wn100bkp/data/dati9mesi52HWI.mat"

#times
PAR_tFft = 8192
PAR_tObs = 9 #mesi
PAR_tObs = PAR_tObs*30*24*60*60
PAR_epoch = (57722+57990)/2 

#frequencies
PAR_enhance = 10
PAR_stepFreq = 1/PAR_tFft
PAR_refinedStepFreq =  PAR_stepFreq/PAR_enhance

#spindowns
PAR_fdotMin = -1e-9
PAR_fdotMax = 1e-10
PAR_stepFdot = PAR_stepFreq/PAR_tObs
PAR_nstepFdot = numpy.round((PAR_fdotMax-PAR_fdotMin)/PAR_stepFdot).astype(numpy.int32)

#others
PAR_secbelt = 4000


#---------------------------------------
# loading and managing data             |
#---------------------------------------
struttura = scipy.io.loadmat(percorsoDati)['job_pack_0']

#times
tempi = struttura['peaks'][0,0][0]
tempi = tempi-PAR_epoch
tempi = ((tempi)*60*60*24/PAR_refinedStepFreq)

#frequencies
frequenze = struttura['peaks'][0,0][1]
freqMin = numpy.amin(frequenze)
freqMax = numpy.amax(frequenze)
freqIniz = freqMin- PAR_stepFreq/2 - PAR_refinedStepFreq
freqFin = freqMax + PAR_stepFreq/2 + PAR_refinedStepFreq
nstepFrequenze = numpy.ceil((freqFin-freqIniz)/PAR_refinedStepFreq)+PAR_secbelt
frequenze = frequenze-freqIniz
frequenze = (frequenze/PAR_refinedStepFreq)-round(PAR_enhance/2+0.001)
 
#spindowns
spindowns = numpy.arange(0, PAR_nstepFdot)
spindowns = numpy.multiply(spindowns,PAR_stepFdot)
spindowns = numpy.add(spindowns, PAR_fdotMin)

#others
pesi = (struttura['peaks'][0,0][4]+1)

peakmap = numpy.stack((tempi,frequenze,pesi),1).astype(numpy.float32)
print(peakmap.shape)
spindowns = spindowns.astype(numpy.float32)
nRows = numpy.int32(PAR_nstepFdot)
nColumns = numpy.int32(nstepFrequenze)

with tf.Session() as sess:

    # load the computation graph
    loader = tf.train.import_meta_graph('./exported/my_model.meta')
    sess.run(tf.global_variables_initializer())
    loader = loader.restore(sess, './exported/my_model')

    x1 = tf.get_default_graph().get_tensor_by_name('inputPM:0')
    x2 = tf.get_default_graph().get_tensor_by_name('inputSD:0')
    t1 = tf.get_default_graph().get_tensor_by_name('output:0')
    
    dizionario = { 
                  x1: peakmap,
                  x2 : spindowns
                 }
    
    start = time.time()
    lalala= sess.run(t1, feed_dict=dizionario)
    end = time.time()
    print(end-start)
    print(tf.global_variables())
    print("out           " ,lalala)

from matplotlib import pyplot
a = pyplot.imshow(lalala, aspect = 100)
pyplot.show()
