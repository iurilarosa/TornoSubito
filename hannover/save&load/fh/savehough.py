import tensorflow as tf
import numpy
import scipy.io
import time



#---------------------------------------
# defining parameters                   |
#---------------------------------------
percorsoDati = "/home/protoss/Documenti/TESI/wn100bkp/data/dati9mesi52HWI.mat"

#times
PAR_tFft = 8192
PAR_tObs = 9 #mesi
PAR_tObs = PAR_tObs*30*24*60*60
PAR_epoch = (57722+57990)/2 

#frequencies
PAR_enhance = 10
PAR_stepFreq = 1/PAR_tFft
PAR_refinedStepFreq =  PAR_stepFreq/PAR_enhance

#spindowns
PAR_fdotMin = -1e-9
PAR_fdotMax = 1e-10
PAR_stepFdot = PAR_stepFreq/PAR_tObs
PAR_nstepFdot = numpy.round((PAR_fdotMax-PAR_fdotMin)/PAR_stepFdot).astype(numpy.int32)

#others
PAR_secbelt = 4000


#---------------------------------------
# loading and managing data             |
#---------------------------------------
struttura = scipy.io.loadmat(percorsoDati)['job_pack_0']

#times
tempi = struttura['peaks'][0,0][0]
tempi = tempi-PAR_epoch
tempi = ((tempi)*60*60*24/PAR_refinedStepFreq)

#frequencies
frequenze = struttura['peaks'][0,0][1]
freqMin = numpy.amin(frequenze)
freqMax = numpy.amax(frequenze)
freqIniz = freqMin- PAR_stepFreq/2 - PAR_refinedStepFreq
freqFin = freqMax + PAR_stepFreq/2 + PAR_refinedStepFreq
nstepFrequenze = numpy.ceil((freqFin-freqIniz)/PAR_refinedStepFreq)+PAR_secbelt
frequenze = frequenze-freqIniz
frequenze = (frequenze/PAR_refinedStepFreq)-round(PAR_enhance/2+0.001)
 
#spindowns
spindowns = numpy.arange(0, PAR_nstepFdot)
spindowns = numpy.multiply(spindowns,PAR_stepFdot)
spindowns = numpy.add(spindowns, PAR_fdotMin)

#others
pesi = (struttura['peaks'][0,0][4]+1)

peakmap = numpy.stack((tempi,frequenze,pesi),1).astype(numpy.float32)
print(peakmap.shape)
spindowns = spindowns.astype(numpy.float32)
nRows = numpy.int32(PAR_nstepFdot)
nColumns = numpy.int32(nstepFrequenze)

#---------------------------------------
# defining TensorFlow graph             |
#---------------------------------------

def mapnonVar(stepIesimo):
    sdTimed = tf.multiply(spindownsTF[stepIesimo], tempiTF, name = "Tdotpert")
    
    appoggio = tf.round(frequenzeTF-sdTimed+PAR_secbeltTF/2, name = "trasfFreq")
    appoggio = tf.cast(appoggio, dtype=tf.int32)
    
    valori = tf.unsorted_segment_sum(pesiTF, appoggio, PAR_nColumnsTF)
    return valori

PAR_secbeltTF = tf.constant(4000, dtype = tf.float32, name = 'secur')
PAR_nRowsTF = tf.constant(PAR_nstepFdot, dtype = tf.int32)
PAR_nColumnsTF = tf.constant(nstepFrequenze, dtype = tf.int32)

peakmapTF = tf.placeholder(tf.float32, name = 'inputPM')
tempiTF = peakmapTF[:,0]
frequenzeTF = peakmapTF[:,1]
pesiTF = peakmapTF[:,2]

spindownsTF = tf.placeholder(tf.float32, name = 'inputSD')

hough = tf.Variable(tf.zeros([PAR_nRowsTF,PAR_nColumnsTF]),name = "var")
hough = hough.assign(tf.map_fn(mapnonVar, tf.range(0, PAR_nRowsTF), dtype=tf.float32, parallel_iterations=8))

houghLeft = tf.slice(hough, [0,0], [hough.get_shape()[0],10])
houghRight = tf.slice(hough, [0,10],[hough.get_shape()[0], hough.get_shape()[1]-10]) - tf.slice(hough, [0,0],[hough.get_shape()[0], hough.get_shape()[1]-10])
houghDiff = tf.concat([houghLeft,houghRight],1)
houghMap = tf.cumsum(houghDiff, axis = 1, name = "output")

sess = tf.Session()

sess.run(tf.global_variables_initializer())

# save graph
saver = tf.train.Saver(tf.global_variables())
saver.save(sess, './exported/my_model')

tf.train.write_graph(sess.graph, '.', "./exported/graph.pb", as_text=False)
#tf.train.write_graph(sess.graph, '.', "./exported/graph.pb_txt", as_text=True)

x1 = tf.get_default_graph().get_tensor_by_name('inputPM:0')
x2 = tf.get_default_graph().get_tensor_by_name('inputSD:0')
t1 = tf.get_default_graph().get_tensor_by_name('output:0')

dizionario = { 
              x1: peakmap,
              x2: spindowns
             }

start = time.time()
lalala= sess.run(t1, feed_dict=dizionario)
end = time.time()
print(end-start)

from matplotlib import pyplot
a = pyplot.imshow(lalala, aspect = 100)
pyplot.show()
