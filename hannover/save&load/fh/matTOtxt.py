import tensorflow as tf
import numpy
import scipy.io
import time

sessione = tf.Session()

#---------------------------------------
# defining parameters                   |
#---------------------------------------
percorsoIn = "/home/protoss/Documenti/TESI/wn100bkp/data/dati9mesi52HWI.mat"

#times
PAR_tFft = 8192
PAR_tObs = 9 #mesi
PAR_tObs = PAR_tObs*30*24*60*60
PAR_epoch = (57722+57990)/2 

#frequencies
PAR_enhance = 10
PAR_stepFreq = 1/PAR_tFft
PAR_refinedStepFreq =  PAR_stepFreq/PAR_enhance

#spindowns
PAR_fdotMin = -1e-9
PAR_fdotMax = 1e-10
PAR_stepFdot = PAR_stepFreq/PAR_tObs
PAR_nstepFdot = numpy.round((PAR_fdotMax-PAR_fdotMin)/PAR_stepFdot).astype(numpy.int32)

#others
PAR_secbelt = 4000


#---------------------------------------
# loading and managing data             |
#---------------------------------------
struttura = scipy.io.loadmat(percorsoIn)['job_pack_0']

#times
tempi = struttura['peaks'][0,0][0]
tempi = tempi-PAR_epoch
tempi = ((tempi)*60*60*24/PAR_refinedStepFreq)

#frequencies
frequenze = struttura['peaks'][0,0][1]
freqMin = numpy.amin(frequenze)
freqMax = numpy.amax(frequenze)
freqIniz = freqMin- PAR_stepFreq/2 - PAR_refinedStepFreq
freqFin = freqMax + PAR_stepFreq/2 + PAR_refinedStepFreq
nstepFrequenze = numpy.ceil((freqFin-freqIniz)/PAR_refinedStepFreq)+PAR_secbelt
frequenze = frequenze-freqIniz
frequenze = (frequenze/PAR_refinedStepFreq)-round(PAR_enhance/2+0.001)
 
#spindowns
spindowns = numpy.arange(0, PAR_nstepFdot)
spindowns = numpy.multiply(spindowns,PAR_stepFdot)
spindowns = numpy.add(spindowns, PAR_fdotMin).astype(numpy.float32)

#others
pesi = (struttura['peaks'][0,0][4]+1)

peakmap = numpy.stack((tempi,frequenze,pesi),1).astype(numpy.float32)
print(peakmap)
spindowns = spindowns.astype(numpy.float32)
PAR_nRows = numpy.int32(PAR_nstepFdot)
PAR_nColumns = numpy.int32(nstepFrequenze)

#---------------------------------------
# writing data                          |
#---------------------------------------

percorsoPM = "peakmap.txt"
percorsoSD = "spindowns.txt"
percorsoPAR = "parametri.txt"

numpy.savetxt(percorsoPM, peakmap)
numpy.savetxt(percorsoSD, spindowns, fmt='%s')
numpy.savetxt(percorsoPAR, [PAR_secbelt, PAR_nRows, PAR_nColumns])

