/*
  Lettura di un file riga per riga.
*/

#include<stdlib.h>
#include<stdio.h>

void caricatutto()
{
// 	FILE *datafile;
	FILE *datafile;
// 	FILE *sdfile;
	int npar = 3;
	float pars[npar];
	float val;
	int i = 0;
	int j;
	int nsd = 210;
	int size = 4739232;
	int npunti = 1579744;
	int narray = 3;
	
	float *dati;
	dati = malloc (sizeof(float) * size);
	float *fdots;
	fdots = malloc (sizeof(float) * nsd+1);
	
	datafile = fopen("parametri.txt", "r");
	if (datafile) 
	{
		i = 0;
		while (!feof(datafile)) 
		{
			fscanf(datafile,"%f", &val);
			pars[i] = val;
			i++;
		}
		fclose(datafile);
	}

	printf("%f, %f, %f\n", pars[0], pars[1], pars[2]);
	
	datafile = fopen("peakmap.txt", "r");
	if (datafile) 
	{
		i = 0;
		while (!feof(datafile)) 
		{
			fscanf(datafile,"%f", &val);
			dati[i] = val;
			i++;
		}
		fclose(datafile);
	}
	printf("%f\t%f\t%f\n", dati[0], dati[1], dati[2]);
	printf("%f\t%f\t%f\n", dati[3], dati[4], dati[5]);
	
	datafile = fopen("spindowns.txt", "r");
	if (datafile) 
	{
		i = 0;
		while (!feof(datafile)) 
		{
			fscanf(datafile,"%f", &val);
			fdots[i] = val;
			i++;
		}
		fclose(datafile);
	}
	printf("%.7e\n", fdots[210]);
}

int main () 
{
	caricatutto();
	return 0;
}

